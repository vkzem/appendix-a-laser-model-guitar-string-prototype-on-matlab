%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% FYP Guitar Synthesizer - Serial Delay Model %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% code takes in various parameters below and runs the bucket brigade delay
% model to generate audio
 
%% 
%parameters for tuning the sound
%loss coefficient
gamma = 0.00005;
 
%transmition coefficient
tau = 1-gamma;
 
%loss/transmit transition matrix
S = [gamma, tau; tau, gamma];
 
%attenuation factor at rigid terminations
att = 0.991;
 
%sampling period
fs = 44100;
 
%fundamental frequency of note
f0 = 330;
 
%string length in samples 
num_of_nodes = ceil(fs/f0)
 
%pluck parameters in normalized string length units
pluck_at = .4
pickup_at = .6
%% 
%initializing the string vectors
vi = zeros([2,num_of_nodes]);
 
vi(1,num_of_nodes*pluck_at:num_of_nodes) = linspace(num_of_nodes,0,num_of_nodes*(1-pluck_at)+1)/num_of_nodes;
vi(1,1:num_of_nodes*pluck_at-1) = linspace(0,num_of_nodes*pluck_at-1,num_of_nodes*pluck_at-1)/(num_of_nodes*(1-pluck_at)-1);
 
vi(2,num_of_nodes*pluck_at:num_of_nodes) = linspace(num_of_nodes,0,num_of_nodes*(1-pluck_at)+1)/num_of_nodes;
vi(2,1:num_of_nodes*pluck_at-1) = linspace(0,num_of_nodes*pluck_at-1,num_of_nodes*pluck_at-1)/(num_of_nodes*(1-pluck_at)-1);
 
%size matrix
[num_of_ports, num_of_nodes] = size(vi);
 
%temporary array to store states of next vi before assignment
temp = zeros([2,num_of_nodes]);
 
%%
%%audio generate
for i = 1:10000*20
    
    %calculate the next state of every node
    vr = S*vi;
    for node = 1:num_of_nodes        
        if node == 1
            temp(1,node) = -att*vr(1,node);
            temp(2,node) = vr(1,node+1);                                
        elseif node == num_of_nodes
            temp(1,node) = vr(2,node-1); 
            temp(2,node) = -att*vr(2,node);           
        else
            temp(1,node) = vr(2,node-1); 
            temp(2,node) = vr(1,node+1);
        end
    end
    
    %set temp
    vi = temp;
 
    
    %sum the two traveling waves
    sig = sum(vi,1);
    
    %pickup output
    z(i) = sig(round(length(sig)*pickup_at));
  
end
    
%%
%plotting/playing back outputs
figure
plot(z)
 
fc = 3000; % Cut off frequency
 
%some filtering to make sound more real
[b,a] = butter(1,fc/(fs/2)); % Butterworth filter of order 6
x = filter(b,a,z(1:end)); 
soundsc(z,44100) %play the filtered signal
